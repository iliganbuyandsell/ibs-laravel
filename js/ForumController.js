(function(){
    var app = angular.module('forum', []).config(function($interpolateProvider, $httpProvider){
        $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
    }).constant("CSRF_TOKEN", '{{ csrf_field() }}');

    app.controller('forumController', function($http, $log){

        users = [];

        this.message;

        var set = this;
        set.data = [];

        set.reply = '';

        var pusher = new Pusher('40e61291036c4a195c67', { encrypted: true } );

        var channel = pusher.subscribe('online_user');
        channel.bind('App\\Events\\OnlineUser', function(data) {

            //this will check if user is exist in the given array.
            if( users.indexOf(data.name) == -1 ){
                users.push(data.name);

                var lists = '';
                for(var i=0; i<users.length; i++){
                    lists += "<li>"+ data.name +"</li>";
                }

                $('#users').html(lists);

            }

        });

        var forum_channel = pusher.subscribe('forum_channel');
        forum_channel.bind('App\\Events\\ForumEvent', function(data) {
            $('#chat').append('<div class="well"><strong>'+data.message_data.name+'</strong> '+data.message_data.message+'</div>');
        });

        $http.get('http://localhost/ibs-laravel/forums/1', {dataType:'json', contentType:"application/json; charset=utf-8"})
        .success(function(results){
            console.log(results.data);
            if(results.status == 'OK'){
                var lists='';
                for(var i=0; i<results.data.length; i++){
                    lists+='<div class="well"><strong>'+results.data[i].user+'</strong> '+results.data[i].message+'</div>';
                }
                $('#chat').html(lists);
            }
        })
        .error(function(exception, cause){
            $log.warn(exception, cause);
        });

        this.sendMessage = function(user_id){
            set.data = {
                "user_id": user_id,
                "message": this.message,
            };

            $http.post('http://localhost/ibs-laravel/forums', {data:set.data, dataType:'json', contentType:"application/json; charset=utf-8"})
            .success(function(results){
                if(results.status == 'OK'){
                    set.reply = results.data.message;
                }
            })
            .error(function(exception, cause){
                $log.warn(exception, cause);
            });

        };

    });

})();
