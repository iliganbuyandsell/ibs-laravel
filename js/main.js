$(document).ready(function(){
	showSections();
	showSectionForSearchItem();
});

function hideAll(){
	$('#sectionloading').hide();
	$('#category_view').hide();
	$('#item-create').hide();
	$('#user-edit').hide();
	$('#user-show').hide();
	$('#item-myitem').hide();
	$('#item-edit').hide();
}

function showSections(){
	$.ajax({

		type:"GET",
		url:"http://localhost/ibs-laravel//section",
		contentType:"application/json; charset=utf-8",
		dataType:"json",

		success: function(results){
			$('#section-list').html(function(){
				var section_row = '';
				var section;
				for (var i = 0; i < results.data.length; i++) {
					section = '<li>'+results.data[i].name+'</li>';
					section_row+=section;
				}
				return section_row;
			});
		},
		error: function(e){
			alert('THIS IS NOT COOL, SOMETHING WENT WRONG: '+e);
		}

	});
}

function showSectionForSearchItem(){
	$.ajax({

		type:"GET",
		url:"http://localhost/ibs-laravel//section",
		contentType:"application/json; charset=utf-8",
		dataType:"json",

		success: function(results){
			$('#select-section').html(function(){
				var section_row = '';
				var section;
				for (var i = 0; i < results.data.length; i++) {
					section = '<option value='+results.data[i].id+'>'+results.data[i].name+'</option>';
					section_row+=section;
				}
				return section_row;
			});
		},
		error: function(e){
			alert('THIS IS NOT COOL, SOMETHING WENT WRONG: '+e);
		}

	});
}

function show_categories(){
	var section_id = $('#section_id').val();
	if(section_id==0){
		hideAll();
	}
	else{
		$('#category_view').hide();
		$('#sectionloading').show();
		$.ajax({

			type:"GET",
			url:"http://localhost/ibs-laravel//category/"+section_id,
			contentType:"application/json; charset=utf-8",
			dataType:"json",
			success: function(results){
				if(results.status == 'OK'){
					$('#category_id').html(function(){
						var category_row = '';
						var category;
						for (var i = 0; i < results.data.length; i++) {
							category = '<option value="'+results.data[i].id+'">'+
										results.data[i].name+
										'</option>';
							category_row+=category;
						}
						return category_row;
					});
				}
				$('#sectionloading').hide();
				$('#category_view').show();
			},
			error: function(e){
				alert('THIS IS NOT COOL. SOMETHING WENT WRONG: '+e);
			}

		});
	}

}

function showItemThumbnail(item_id){
	$.ajax({
		type:"GET",
		url:"http://localhost/ibs-laravel//thumbnail/"+item_id,
		contentType:"application/json; charset=utf-8",
		dataType:"json",
		success: function(results){
			if (results.data) {
				console.log(results.data.name);
				return results.data.name;
			}
		},
		error: function(e){
			alert('THIS IS NOT COOL. SOMETHING WENT WRONG: '+e);
		}
	});
}

function showItemBySection(){
	hideAll();
	$('#search-from-item-create').show();
	var section = $('#select-section').val();
	$.ajax({
		type:"GET",
		url:"http://localhost/ibs-laravel//sections/"+section,
		contentType:"application/json; charset=utf-8",
		dataType:"json",
		success: function(results){

			if (results.data.data) {
				var selling_item_row;
				var selling_item;
				var selling_thumbnail_name;
				var selling_posted_by;

				var buying_item_row;
				var buying_item;
				var buying_thumbnail_name;
				var buying_posted_by;

				$('#selling-section').html(function(){
					if (!results.data.total) {
						return '<p class="media-heading">No item found</p>';
					}
					for (var i = 0; i < results.data.total; i++) {
						if (results.data.data[i].type_id == 2) {

							for (var j = 0; j < results.thumbnail_data.length; j++) {
								if (results.thumbnail_data[j] && results.thumbnail_data[j].item_id == results.data.data[i].id) {
									selling_thumbnail_name = results.thumbnail_data[j].name;
								}
							}
							for (var k = 0; k < results.user_data.length; k++) {
								if (results.user_data[k] && results.user_data[k].id == results.data.data[i].user_id) {
									selling_posted_by = results.user_data[k].name;
								}
							}
							selling_item = '<div class="media">'+
								   '<div class="media-left">'+
								    '<a href="http://localhost/ibs-laravel//item/' + results.data.data[i].id + '">'+
										'<img class="media-object" data-src="http://localhost/ibs-laravel//www/public/img/'
										+selling_thumbnail_name+
										'" alt="Description Here" src="http://localhost/ibs-laravel//www/public/img/'
										+selling_thumbnail_name+
										'" width="64" height="64">'+
									'</a>'+
									'</div>'+
									'<div class="media-body">'+
									'<h4 class="media-heading"><a href="http://localhost/ibs-laravel//item/' + results.data.data[i].id + '">' + results.data.data[i].name + '</a></h4>'
									+'<p>'+results.data.data[i].description+'</p>'+
									'<small><em>'+
									'<span class="price">₱ '+results.data.data[i].price+'</span>Posted by: '
									+'<span class="posted_by">'+selling_posted_by+'</span>Created at: '+
									'<span class="created_at">'+results.data.data[i].created_at+'</span>'
									+'</em></small>'
								  +'</div>'
								  +'</div>';
							selling_item_row+=selling_item;
						}
					}
					if (!selling_item_row) {
						return '<p class="media-heading">No item found</p>';
					}
					return selling_item_row;
				});

				$('#buying-section').html(function(){
					if (!results.data.total) {
						return '<p class="media-heading">No item found</p>';
					}
					for (var i = 0; i < results.data.total; i++) {
						if (results.data.data[i].type_id == 1) {
							for (var j = 0; j < results.thumbnail_data.length; j++) {
								if (results.thumbnail_data[j] && results.thumbnail_data[j].item_id == results.data.data[i].id) {
									buying_thumbnail_name = results.thumbnail_data[j].name;
								}
							}
							for (var k = 0; k < results.user_data.length; k++) {
								if (results.user_data[k] && results.user_data[k].id == results.data.data[i].user_id) {
									buying_posted_by = results.user_data[k].name;
								}
							}
							buying_item = '<div class="media">'+
								   '<div class="media-left">'+
								    '<a href="http://localhost/ibs-laravel//item/' + results.data.data[i].id + '">'+
										'<img class="media-object" data-src="http://localhost/ibs-laravel//www/public/img/'
										+buying_thumbnail_name+
										'" alt="Description Here" src="http://localhost/ibs-laravel//www/public/img/'
										+buying_thumbnail_name+
										'" width="64" height="64">'+
									'</a>'+
									'</div>'+
									'<div class="media-body">'+
									'<h4 class="media-heading"><a href="http://localhost/ibs-laravel//item/' + results.data.data[i].id + '">' + results.data.data[i].name + '</a></h4>'
									+'<p>'+results.data.data[i].description+'</p>'+
									'<small><em>'+
									'<span class="price">₱ '+results.data.data[i].price+'</span>Posted by: '
									+'<span class="posted_by">'+buying_posted_by+'</span>Created at: '+
									'<span class="created_at">'+results.data.data[i].created_at+'</span>'
									+'</em></small>'
								  +'</div>'
								  +'</div>';
							buying_item_row+=buying_item;
						}
					}
					if (!buying_item_row) {
						return '<p class="media-heading">No item found</p>';
					}
					return buying_item_row;
				});
			}
		},
		error: function(e){
			alert('THIS IS NOT COOL. SOMETHING WENT WRONG: '+e);
		}
	});
}
