<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ForumEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $message_data;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message_data)
    {
        $this->message_data = $message_data;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['forum_channel'];
    }
}
