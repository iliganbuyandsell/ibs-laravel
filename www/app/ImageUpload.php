<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageUpload extends Model
{
    protected $table = 'image';
    protected $fillable = array('name', 'directory', 'created_at', 'updated_at', 'item_id');

    public function item(){
    	return $this->belongsTo('App\Item');
    }
}
