<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// DEFINE CONSTANT
define( "SITE_ABRE" , "IBS");
define( "SITE_NAME" , "Iligan Buy and Sell");


// PAGES
Route::get('/', [
	'uses' => '\App\Http\Controllers\ItemController@index',
	'as' => 'home'
]);

Route::get('/about', function () {

    return view('about')
    		->with('pageTitle', SITE_ABRE . ' | About Us');

});

Route::get('/welcome', function () {

    return view('welcome');
    		//->with('pageTitle', SITE_ABRE . ' | About Us');

});


Route::group(['middleware' => ['auth'] ], function(){

	Route::resource('item', 'ItemController');

	Route::resource('type', 'TypeController');

	Route::resource('user', 'UserController', ['except' => ['create', 'store', 'show']]);
	Route::get('/myitem/{user_id}', 'ItemController@showMyItem');
	Route::post('/myitem/{user_id}', 'ItemController@showMyItem');

	Route::resource('comment.item.user', 'CommentController', [

		'parameters' => ['item'=>'item_id', 'user'=>'user_id']

		]);

	Route::resource('forums', 'ForumController');

});

Route::resource('category', 'CategoryController');

Route::resource('section', 'SectionController');

Route::resource('thumbnail', 'ThumbnailController');

//user resource must be define outside the group because the user.create route is called in the index.blade
Route::resource('user', 'UserController', ['only' => ['create', 'store', 'show']]);

Route::get('/search', 'ItemController@search');

Route::get('/sections/{section_id}', 'ItemController@showBySection');



//FACEBOOK
Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');
Route::post('/fblogin', 'SocialAuthController@store');

//Simple Authentication
Route::post('/login', [
	'uses' => '\App\Http\Controllers\Auth\AuthController@postLogin',
	'as' => 'auth.login'
]);

Route::get('/logout', [
	'uses' => '\App\Http\Controllers\Auth\AuthController@getLogout',
	'as' => 'logout'
]);
