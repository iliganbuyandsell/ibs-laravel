<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Validator;
use App\Http\Controllers\Controller;
Use Socialite;
use App\Item;
use App\User;
use App\Type;
use App\Category;
use App\ImageUpload;
use Auth;
use DateTime;
use Hash;
use Illuminate\Support\Facades\Redirect;

class SocialAuthController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();   
    }   

    public function callback()
    {


        try{
            $user = Socialite::driver('facebook')->user();
            $check_user_if_exist = User::where('facebook_id', '=', $user->getId() )
                                        //check if facebook_id is exist and also username
                                        //because in logging in in with fb (assume the guest user is not registered)
                                        //automatically the email(from fb) used as the username for ibs account.
                                       ->orWhere('username',  $user->getEmail() )
                                        ->get()->first();


            if($check_user_if_exist){

                Auth::loginUsingId( $check_user_if_exist->id );
                return redirect()->action('ItemController@index')
                                 ->with('status','loggedin')
                                 ->withSuccess('Successfully logged in ' . $check_user_if_exist->name);

            }

            if(!$check_user_if_exist){
                return view('facebook_register')
                            ->with('fb_id', $user->getId() )
                            ->with('fb_name', $user->getName() )
                            ->with('fb_avatar', $user->getAvatar() )
                            ->with('fb_email', $user->getEmail() )
                            ->with('fb_token', $user->token)
                            ->with('pageTitle', SITE_ABRE . ' | Facebook Register');
            }

        }

        catch(Exception $ex){
            return $ex;
        }

   
    }

    public function store(Request $request){

        $user = new User;
        $now = new DateTime();
        $timestamp = $now->getTimestamp();

        $this->validate($request, [
            'username' => 'required|unique:user|max:255',
            'facebook_id' => 'required|unique:user',
            'email' => 'required|unique:user',
        ]);

        $user->username = $request->input('username');
        $user->facebook_id = $request->input('facebook_id');
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->remember_token = $request->input('token');
        $user->role_id = 2;
        $user->city_id = 792;
        $user->created_at = $timestamp;
        $user->save();

        Auth::loginUsingId($user->id);

        // $pass = Hash::make('123');
        $for_sell = Item::where('type_id', 2)->orderBy('updated_at', 'desc')->paginate(15);
        $looking_for = Item::where('type_id', 1)->orderBy('updated_at', 'desc')->paginate(15);

        $images = ImageUpload::get();

        return redirect()->action('ItemController@index')
                         ->with('status','loggedin')
                         ->withSuccess('Successfully logged in ' . $user->name);

    }
}
