<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Item;
use App\User;
use App\Thumbnail;
use App\Type;
use App\Category;
use App\Section;
use App\ImageUpload;
use App\Comment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DateTime;
use Illuminate\Support\Facades\Redirect;
use Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;


// import the Intervention Image Manager Class
use Intervention\Image\ImageManagerStatic as Image;

// configure with favored image driver (gd by default)
Image::configure(array('driver' => 'gd'));

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $for_sell = Item::where('type_id', 2)
                        ->where('is_active', true)
                        ->orderBy('updated_at', 'desc')
                        ->paginate(15);

        $looking_for = Item::where('type_id', 1)
                            ->where('is_active', true)
                            ->orderBy('updated_at', 'desc')
                            ->paginate(15);

        $thumbnails = Thumbnail::get();
        $user = Auth::user();
        // $pass = Hash::make('123');

        if ( $user ) {

            return view('index', ['for_sell'=>$for_sell, 'looking_for'=>$looking_for, 'user'=>$user, 'thumbnails'=>$thumbnails])
                ->with('pageTitle', SITE_NAME . ' | The best place to Buy a house, Sell a car or Find a job in Iligan City.')
                ->with('user', $user);
                // ->with('pass', $pass);
            //return $pass;
        }

        else {

            return view('welcome', ['for_sell'=>$for_sell, 'looking_for'=>$looking_for, 'user'=>$user, 'thumbnails'=>$thumbnails])
                ->with('pageTitle', SITE_NAME . ' | The best place to Buy a house, Sell a car or Find a job in Iligan City.')
                ->with('user', $user);
                // ->with('pass', $pass);
            //return $pass;
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Type::all();
        $sections = Section::all();
        $user = Auth::user();

        return view('item.create', ['types'=>$types, 'sections'=>$sections, 'user'=>$user])
            ->with('pageTitle', SITE_ABRE . ' | Create Item');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = new Item;
        $now = new DateTime();
        $user = Auth::user();
        $timestamp = $now->getTimestamp();

        $this->validate($request, [
            'name' => 'required|max:255',
            'type_id' => 'required',
            'category_id' => 'required',
        ]);

        $item->name = $request->input('name');
        $item->price = $request->input('price');
        $item->description = $request->input('description');
        $item->created_at = $timestamp;
        $item->updated_at = $timestamp;
        $item->type_id = $request->input('type_id');
        $item->category_id = $request->input('category_id');
        $item->user_id = $user->id;
        $item->save();

        //creating the image
        $images = Input::file('images');

        if(!$images){
            $items = Item::orderBy('updated_at', 'desc')->where('is_active', true)->get();

            return Redirect::back()->withSuccess('Successfully added item ' . $item->name)
                    ->with('user', $user);
        }

        $filename  = time() . '.' . $images->getClientOriginalExtension();
        $thumbnail_name = 'thumbnail' . time() . '.' . $images->getClientOriginalExtension();
        $path = public_path('img/' . $filename);
        $thumbnail_path = public_path('img/' . $thumbnail_name);
        Image::make($images->getRealPath())->pixelate(1)->resize(304,236)->save($path);
        Image::make($images->getRealPath())->pixelate(1)->resize(64,64)->save($thumbnail_path);

        $image = new ImageUpload;
        $thumbnail = new Thumbnail;

        $image->name = $filename;
        $image->directory = $filename;
        $image->created_at = $timestamp;
        $image->updated_at = $timestamp;
        $image->item_id = $item->id;

        $thumbnail->name = $thumbnail_name;
        $thumbnail->item_id = $item->id;
        $thumbnail->created_at = $timestamp;
        $thumbnail->updated_at = $timestamp;

        // $image_make->fit(300, 200);

        // $image_make->save('../resources/views/item/images/'. $image_name);

        $image->save();
        $thumbnail->save();


        $items = Item::orderBy('updated_at', 'desc')->where('is_active', true)->get();

        return Redirect::back()->withSuccess('Successfully added item ' . $item->name)
                ->with('user', $user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Item::where('id', '=' , $id)->where('is_active', true)->get()->first();
        $images = ImageUpload::where('item_id', $item->id)->get()->first();
        $user = Auth::user();
        $comments = Comment::orderBy('created_at', 'desc')->where('item_id', $id)->get();

        return view('item.show', ['item'=>$item, 'user'=>$user, 'comments'=>$comments, 'images'=>$images])
            ->with('pageTitle', SITE_ABRE . ' | View Item');
    }

    public function showMyItem($user_id)
    {
        $items = Item::where('user_id', '=' , $user_id)->where('is_active', true)->get();
        $user = Auth::user();
        $thumbnails = Thumbnail::get();

        return view('item.myItem', ['items'=>$items, 'user'=>$user, 'thumbnails'=>$thumbnails])
            ->with('pageTitle', SITE_ABRE . ' | My Items');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // if(Auth::check()){
        //     $item = Item::where('id', $id)->where('is_active', true)->get()->first();
        //     $images = ImageUpload::where('item_id', $id)->where('is_active', true)->get()->first();
        //     $user = Auth::user();

        //     return view('item.edit')
        //             ->with('item', $item)
        //             ->with('images', $images)
        //             ->with('user', $user);
        // }

        $item = Item::where('id', $id)->where('is_active', true)->get()->first();
        $images = ImageUpload::where('item_id', $id)->get()->first();
        $types = Type::get();
        $sections = Section::get();

        return view('item.edit')
                    ->with('item', $item)
                    ->with('types', $types)
                    ->with('images', $images)
                    ->with('sections', $sections)
                    ->with('pageTitle', SITE_ABRE . ' | Update Item');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $now = new DateTime();
        $timestamp = $now->getTimestamp();

        $this->validate($request, [
            'name' => 'required|max:255',
            'type_id' => 'required',
            'category_id' => 'required',
        ]);

        $item = Item::where('id', $id)->get()->first();
        $item->type_id = $request->input('type_id');
        $item->name = $request->input('name');
        $item->description = $request->input('description');
        $item->category_id = $request->input('category_id');
        $item->price = $request->input('price');
        $item->updated_at = $timestamp;
        $item->save();


        $items = Item::where('user_id', Auth::user()->id)->get();
        $user = User::where('id', Auth::user()->id)->get();
        $thumbnails = Thumbnail::get();

        if (Input::file('images')) {
            $img = ImageUpload::where('item_id', $item->id)->get()->first();
            if ($img) {
                Storage::delete($image->name);
                $images = Input::file('images');

                $filename  = time() . '.' . $images->getClientOriginalExtension();
                $thumbnail_name = 'thumbnail' . time() . '.' . $images->getClientOriginalExtension();
                $path = public_path('img/' . $filename);
                $thumbnail_path = public_path('img/' . $thumbnail_name);
                Image::make($images->getRealPath())->pixelate(1)->resize(304,236)->save($path);
                Image::make($images->getRealPath())->pixelate(1)->resize(64,64)->save($thumbnail_path);

                $image = new ImageUpload;
                $thumbnail = new Thumbnail;

                $image->name = $filename;
                $image->directory = $filename;
                $image->created_at = $timestamp;
                $image->updated_at = $timestamp;
                $image->item_id = $item->id;

                $thumbnail->name = $thumbnail_name;
                $thumbnail->item_id = $item->id;
                $thumbnail->created_at = $timestamp;
                $thumbnail->updated_at = $timestamp;

                // $image_make->fit(300, 200);

                // $image_make->save('../resources/views/item/images/'. $image_name);

                $image->save();
                $thumbnail->save();

                return 'exist image';

            }

            $now = new DateTime();
            $timestamp = $now->getTimestamp();

            $images = Input::file('images');

            $filename  = time() . '.' . $images->getClientOriginalExtension();
            $thumbnail_name = 'thumbnail' . time() . '.' . $images->getClientOriginalExtension();
            $path = public_path('img/' . $filename);
            $thumbnail_path = public_path('img/' . $thumbnail_name);
            Image::make($images->getRealPath())->pixelate(1)->resize(304,236)->save($path);
            Image::make($images->getRealPath())->pixelate(1)->resize(64,64)->save($thumbnail_path);

            $image = new ImageUpload;
            $thumbnail = new Thumbnail;

            $image->name = $filename;
            $image->directory = $filename;
            $image->created_at = $timestamp;
            $image->updated_at = $timestamp;
            $image->item_id = $item->id;

            $thumbnail->name = $thumbnail_name;
            $thumbnail->item_id = $item->id;
            $thumbnail->created_at = $timestamp;
            $thumbnail->updated_at = $timestamp;

            $image->save();
            $thumbnail->save();

            return view('item.myItem', ['items'=>$items, 'user'=>$user, 'thumbnails'=>$thumbnails])
                ->with('pageTitle', SITE_ABRE . ' | My Items')
                ->with('info', 'Successfully updated!');
        }


        return view('item.myItem', ['items'=>$items, 'user'=>$user, 'thumbnails'=>$thumbnails])
            ->with('pageTitle', SITE_ABRE . ' | My Items')
            ->with('info', 'Successfully updated!');
    }

    public function search(Request $request)
    {
        $query = $request->input('query');
        $thumbnails = Thumbnail::get();

        $for_sell = Item::where('type_id', 2)
                    ->where('name', 'LIKE', '%'.$query.'%')
                    ->where('is_active', true)
                    ->orderBy('updated_at', 'desc')
                    ->paginate(15);
        $looking_for = Item::where('type_id', 1)
                        ->where('name', 'LIKE', '%'.$query.'%')
                        ->where('is_active', true)
                        ->orderBy('updated_at', 'desc')
                        ->paginate(15);

        $images = ImageUpload::get();
        $user = Auth::user();
        // $pass = Hash::make('123');

        return view('index', ['for_sell'=>$for_sell, 'looking_for'=>$looking_for, 'user'=>$user, 'images'=>$images, 'thumbnails'=>$thumbnails])
            ->with('pageTitle', SITE_ABRE . ' | Section')
            ->with('user', $user);
    }

    public function showBySection($id){
        $section = Section::find($id);
        $thumbnail_data = array();
        $user_data = array();
        //if you want to paginate an object use lentthaware
        $paginator = new LengthAwarePaginator($section->item, count($section->item), 15);

        foreach ($section->item as $s) {
            array_push($thumbnail_data,Thumbnail::where('item_id', $s->id)->first());
            array_push($user_data,User::where('id', $s->user_id)->first());
        }
        return response()->json(['status'=>'OK','data'=>$paginator, 'message'=>'Successfully getting itms by section', 'thumbnail_data'=>$thumbnail_data, 'user_data'=>$user_data]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::check()){
            $item = Item::where('id', $id)->where('is_active', true)->first();

            if($item){
                $item->is_active = false;
                $item->save();
                return Redirect::back()->withSuccess('Successfully deleted item '.$item->name);
            }

            else{
                return "ERROR EDITING ";
            }
        }

        else{
            return "Please log in";
        }
    }
}
