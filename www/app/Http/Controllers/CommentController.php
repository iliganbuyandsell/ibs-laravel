<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Comment;
use App\Type;
use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DateTime;
use Illuminate\Support\Facades\Redirect;
use Hash;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $item_id, $user_id)
    {
        $comment = new Comment;
        $now = new DateTime();
        $timestamp = $now->getTimestamp();

        if(Auth::check()){
            $user = Auth::user();

            $comment->comment = $request->input('comment');
            $comment->created_at = $timestamp;
            $comment->user_id = $user_id;
            $comment->item_id = $item_id;
            $comment->save();

            $comments = Comment::orderBy('created_at', 'desc')->where('item_id', $item_id)->get();
            $item = Item::where('id', $item_id)->get()->first();

            $item->updated_at = $timestamp;
            $item->save();

            return Redirect::back()
                ->with('user', $user)
                ->with('comments', $comments)
                ->with('item', $item)
                ->with('pageTitle', SITE_ABRE . ' | View Item');

        }

        return 'ERROR 404';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
