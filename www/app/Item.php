<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'item';
    protected $fillable = array('name', 'price', 'description', 'created_at', 'updated_at', 'type_id',
    							'category_id', 'user_id', 'is_active');

    public function user(){
    	return $this->belongsTo('App\User');
    }
    public function category(){
    	return $this->belongsTo('App\Category');
    }
    public function type(){
    	return $this->belongsTo('App\Type');
    }
}
