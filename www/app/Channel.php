<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $table = 'channel';
    protected $fillable = array('name',);

    public function message(){
        return $this->hasManyThrough('App\Message', 'App\ChannelUser');
    }
}
