@extends('layout.default')

@section('content')

	<div class="container-fluid">

		<div class="row">

			<div class="col-md-10">

				<div class="row">

					<div class="col-md-12">

						<div class="alert alert-dismissible alert-info">
						  <!-- <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button> -->
						  <h4>Welcome to IBS | Iligan Buy and Sell!</h4>
						  <p>The best place to Buy a house, Sell a car or Find a job in Iligan City.</p>
						</div>


					</div>

				</div>

				<div class="row">

					{{-- Selling Section / Categories Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-default">
		  				@if(Auth::guest())
							<div class="panel-heading">
							    <h3 class="panel-title">Iligan Buy and Sell Registration</h3>
							</div>
							<div class="panel-body">
							    	@if( Session::has( 'success' ))
							    	<div class="alert alert-success">
									     {{ Session::get( 'success' ) }}
									</div>

									@elseif($errors)
								     	@foreach($errors->all() as $error)
								     		<div class="alert alert-danger">
								     			{{ $error }}
								     		</div>
								     	@endforeach
									@endif


							    <form method="POST" action="{{route('user.store')}}">

							    	{!! csrf_field() !!}

							    	<div class="form-group">
									    <label>Name</label>
									   	<input type="text" class="form-control" name="name" placeholder="">
									</div>

									<div class="form-group">
										<label>Email</label>
    									<input type="text" class="form-control" name="email" placeholder="">
    								</div>

    								<div class="form-group">
										<label>Username</label>
    									<input type="text" class="form-control" name="username" placeholder="">
    								</div>

    								<div class="form-group">
									    <label>Password</label>
									   	<input type="password" class="form-control" name="password" placeholder="">
									</div>

    								<div class="form-group">
    									<button type="submit" class="btn btn-primary">Create my Account!</button>
    								</div>

							    </form>

							</div>

						@elseif(Auth::check())
					    <div class="alert alert-warning">
						  <strong>Warning!</strong> You're currently logged in.
						</div>
						@endif
						</div>



		  			</div>
		  			{{-- //Selling Section / Categories Section--}}

		  			{{-- Buying Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-warning">
							<div class="panel-heading">
							    <h3 class="panel-title">Basic Posting Item Guideline</h3>
							</div>
							<div class="panel-body">
							    <p>You can list as many items as you want in the buy/sell section. Here are the requirements and recommendations for adding product images, descriptions and variants.</p>
							    <ul>

							    	<li>At least one image is required for each product. Are easy to understand and show the whole product</li>
							    	<li>Description, should be rich text only (no HTML). Provide only information directly related to the product. Make use of short sentences.</li>
							    	<li>You will not post items in the a section that do not fall under any of the defined categories.</li>
							    	<li>By posting items in the IBS, you guarantee that your item has been legally purchased or acquired. IBS will not be liable for any false, misleading or inaccurate representations by its users.</li>

							    </ul>
							</div>
						</div>



		  			</div>
		  			{{-- //Buying Section --}}

				</div>

			</div>

			<div class="col-md-2">

					@include('layout.left-side')

			</div>

		</div>

	</div>

@endsection
