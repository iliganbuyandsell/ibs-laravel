@extends('layout.lte-default')

@section('content')

	<div class="container-fluid">


		<div id="search-from-item-create" style="display:none" class="row">

			<div class="col-md-12">

				<div class="row">

					{{-- Selling Section / Categories Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-success">
							<div class="panel-heading">
							    <h3 class="panel-title">Selling Section</h3>
							</div>
							<div id="selling-section" class="panel-body ibs-panel-body">

							</div>
						</div>



		  			</div>
		  			{{-- //Selling Section / Categories Section--}}

		  			{{-- Buying Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-info">
							<div class="panel-heading">
							    <h3 class="panel-title">Buying Section</h3>
							</div>
							<div id="buying-section" class="panel-body ibs-panel-body">

							</div>
						</div>



		  			</div>
		  			{{-- //Buying Section --}}

				</div>

			</div>


		</div>

		<div id="user-edit" class="row">

			<div class="col-md-10">

				<div class="row">

					{{-- Selling Section / Categories Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-default">
		  				@if(Auth::check())
							<div class="panel-heading">
							    <h3 class="panel-title">Iligan Buy and Sell Registration</h3>
							</div>
							<div class="panel-body">
							    	@if( Session::has( 'success' ))
							    	<div class="alert alert-success">
									     {{ Session::get( 'success' ) }}
									</div>
									@elseif( $errors )
										@foreach($errors->all() as $error)
											<div class="alert alert-danger">
											     {{ $error }} <!-- here to 'withWarning()' -->
											</div>
										@endforeach
									@endif


							    <form method="POST" action="{{route('user.update', Auth::user()->id)}}">

							    	{!! csrf_field() !!}
							    	{!! method_field('put') !!}

							    	<div class="form-group">
									    <label>Name</label>
									   	<input type="text" class="form-control" name="name" value="{{$user->name}}" required="">
									</div>

									<div class="form-group">
										<label>Email</label>
    									<input type="text" class="form-control" name="email" value="{{$user->email}}" required="">
    								</div>

    								<div class="form-group">
										<label>Username</label>
    									<input type="text" class="form-control" name="username" value="{{$user->username}}" required>
    								</div>

    								<div class="form-group">
										<label>Contact</label>
    									<input type="text" class="form-control" name="contact" value="{{$user->contact}}">
    								</div>

    								<div class="form-group">
										<label>Address</label>
    									<input type="text" class="form-control" name="address" value="{{$user->address}}">
    								</div>

    								<div class="form-group">
									    <label>Password</label>
									   	<input type="password" class="form-control" name="password">
									</div>

									<div class="form-group">
									    <label>Confirm Password</label>
									   	<input type="password" class="form-control" name="password_confirm">
									</div>

    								<div class="form-group">
    									<button type="submit" class="btn btn-primary">Update my Account!</button>
    								</div>

							    </form>

							</div>

						@elseif(Auth::guest())
					    <div class="alert alert-warning">
						  <strong>Warning!</strong> Please logged in.
						</div>
						@endif
						</div>



		  			</div>
		  			{{-- //Selling Section / Categories Section--}}

				</div>

			</div>

			<div class="col-md-2">

					@include('layout.left-side')

			</div>

		</div>

	</div>

@endsection
