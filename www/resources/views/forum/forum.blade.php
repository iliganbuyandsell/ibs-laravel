@extends('layout.lte-default')

@section('content')

	<div ng-app="forum" class="container-fluid">


            <script src="https://js.pusher.com/3.1/pusher.min.js"></script>

            <!-- angular -->
            <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js" crossorigin="anonymous"></script>
            <!-- angular controller -->
            <script src="{{asset("/js/ForumController.js")}}"></script>

            <div ng-controller="forumController as fc" id="messageArea" class="row">
                <div class="col-md-4">
                    <div class="well">
                        <h3>Online Users</h3>
                        <ul class="list-group" id="users">

                        </ul>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="chat" id="chat">

                    </div>
                    <form id="messageForm">
                        <div class="form-group">
                            <label>Enter Message</label>
                            <textarea ng-model="fc.message" class="form-control" id="message"></textarea>
                            <br>
                                <input type="button" class="btn btn-primary" value="Send Message" ng-click="fc.sendMessage({{Auth::user()->id}})">
                        </div>
                    </form>
                </div>
            </div>


	</div>

@endsection
