@extends('layout.lte-default')

@section('content')

	<div class="container-fluid">

		<div id="search-from-item-create" style="display:none" class="row">

			<div class="col-md-12">

				<div class="row">

					{{-- Selling Section / Categories Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-success">
							<div class="panel-heading">
							    <h3 class="panel-title">Selling Section</h3>
							</div>
							<div id="selling-section" class="panel-body ibs-panel-body">

							</div>
						</div>



		  			</div>
		  			{{-- //Selling Section / Categories Section--}}

		  			{{-- Buying Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-info">
							<div class="panel-heading">
							    <h3 class="panel-title">Buying Section</h3>
							</div>
							<div id="buying-section" class="panel-body ibs-panel-body">

							</div>
						</div>



		  			</div>
		  			{{-- //Buying Section --}}

				</div>

			</div>


		</div>

		<div id="item-myitem" class="row">

			<div class="col-md-10">

				<div class="row">

					@if( Session::has( 'info' ))
					<div class="alert alert-danger">
						 {{ Session::get( 'info' ) }} <!-- here to 'withWarning()' -->
					</div>
					@endif

					{{-- Selling Section / Categories Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-success">
							<div class="panel-heading">
							    <h3 class="panel-title">Your For Sale Item(s)</h3>
							</div>
							<div class="panel-body ibs-panel-body">

								@if( count($items) == 0  )
									<p>There is no item in your inventory.</p>
								@endif

								@foreach($items as $item)
									@if ($item->type_id == 2)

										<div class="media">

											<div class="media-left">
												<a href="{{ route('item.show', $item->id) }}">
													@foreach($thumbnails as $thumbnail)
													@if($thumbnail->item_id == $item->id)
													<img class="media-object" data-src="{{ asset('/www/public/img/'.$thumbnail->name) }}" alt="Description Here" src="{{ asset('/www/public/img/'.$thumbnail->name) }}" width="64" height="64">
													@endif()
													@endforeach
												</a>
											</div>

											<div class = "media-body">
												<h4 class = "media-heading"><a href="{{ route('item.show', $item->id) }}">{{$item->name}}</a>

												<form method="post" action="{{ route('item.destroy', $item->id) }}">
													{!! csrf_field() !!}
													{!! method_field('delete') !!}
													<button class="btn btn-warning btn-mini" type="submit">Delete</button>
												</form>
												<form method="get" action="{{ route('item.edit', $item->id) }}">
													{!! csrf_field() !!}
													{!! method_field('edit') !!}
													<button class="btn btn-success btn-mini" type="submit">Update</button>
												</form>

												</h4>

												<p>{{ $item->description }}</p>
												<small>
													<em>
														<span class="price">Php {{ $item->price }}</span>
														Posted by: <span class="posted_by">{{ $item->user->name }}</span>
														Created at: <span class="created_at">{{ date_format($item->created_at,"F j, Y g:i a") }}</span>
													</em>
												</small>
											</div>

										</div>

									@endif
								@endforeach
							</div>
						</div>



		  			</div>
		  			{{-- //Selling Section / Categories Section--}}

		  			{{-- Buying Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-info">
							<div class="panel-heading">
							    <h3 class="panel-title">You are Looking For This Item(s)</h3>
							</div>
							<div class="panel-body ibs-panel-body">

								@if( count($items) == 0  )
									<p>There is no item in your inventory.</p>
								@endif

							    @foreach($items as $item)
									@if ($item->type_id == 1)

										<div class="media">

											<div class="media-left">
												<a href="{{ route('item.show', $item->id) }}">
													@foreach($thumbnails as $thumbnail)
													@if($thumbnail->item_id == $item->id)
													<img class="media-object" data-src="{{ asset('/www/public/img/'.$thumbnail->name) }}" alt="Description Here" src="{{ asset('/www/public/img/'.$thumbnail->name) }}" width="64" height="64">
													@endif()
													@endforeach
												</a>
											</div>

											<div class = "media-body">
												<h4 class = "media-heading"><a href="{{ route('item.show', $item->id) }}">{{$item->name}}</a>

												<form method="post" action="{{ route('item.destroy', $item->id) }}">
													{!! csrf_field() !!}
													{!! method_field('delete') !!}
													<button class="btn btn-warning btn-mini" type="submit">Delete</button>
												</form>
												<form method="get" action="{{ route('item.edit', $item->id) }}">
													{!! csrf_field() !!}
													{!! method_field('edit') !!}
													<button class="btn btn-success btn-mini" type="submit">Update</button>
												</form>

												</h4>
												<p>{{ $item->description }}</p>
												<small>
													<em>
														<span class="price">Php {{ $item->price }}</span>
														Posted by: <span class="posted_by">{{ $item->user->name }}</span>
														Created at: <span class="created_at">{{ date_format($item->created_at,"F j, Y g:i a") }}</span>
													</em>
												</small>
											</div>

										</div>

									@endif
								@endforeach
							</div>
						</div>



		  			</div>
		  			{{-- //Buying Section --}}

				</div>

			</div>

			<div class="col-md-2">

					@include('layout.left-side')

			</div>

		</div>

	</div>

@endsection
