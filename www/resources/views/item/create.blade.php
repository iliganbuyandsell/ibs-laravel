@extends('layout.lte-default')

@section('content')

	<div class="container-fluid">

		<div id="search-from-item-create" style="display:none" class="row">

			<div class="col-md-12">

				<div class="row">

					{{-- Selling Section / Categories Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-success">
							<div class="panel-heading">
							    <h3 class="panel-title">Selling Section</h3>
							</div>
							<div id="selling-section" class="panel-body ibs-panel-body">

							</div>
						</div>



		  			</div>
		  			{{-- //Selling Section / Categories Section--}}

		  			{{-- Buying Section --}}
		  			<div class="col-md-6">

		  				<div class="panel panel-info">
							<div class="panel-heading">
							    <h3 class="panel-title">Buying Section</h3>
							</div>
							<div id="buying-section" class="panel-body ibs-panel-body">

							</div>
						</div>



		  			</div>
		  			{{-- //Buying Section --}}

				</div>

			</div>


		</div>

		<div id="item-create" class="row">

			<div class="col-md-10">

				<div class="row">

		  			<div class="col-md-6">

		  				<div class="panel panel-warning">
							<div class="panel-heading">
							    <h3 class="panel-title">Basic Posting Item Guideline</h3>
							</div>
							<div class="panel-body">
							    <p>You can list as many items as you want in the buy/sell section. Here are the requirements and recommendations for adding product images, descriptions and variants.</p>
							    <ul>

							    	<li>At least one image is required for each product. Are easy to understand and show the whole product</li>
							    	<li>Description, should be rich text only (no HTML). Provide only information directly related to the product. Make use of short sentences.</li>
							    	<li>You will not post items in the a section that do not fall under any of the defined categories.</li>
							    	<li>By posting items in the IBS, you guarantee that your item has been legally purchased or acquired. IBS will not be liable for any false, misleading or inaccurate representations by its users.</li>

							    </ul>
							</div>
						</div>



		  			</div>

		  			<div class="col-md-6">

		  				<div class="panel panel-default">
		  				@if(Auth::check())
							<div class="panel-heading">
							    <h3 class="panel-title">Item Information</h3>
							</div>
							<div class="panel-body">
							    	@if( Session::has( 'success' ))
							    	<div class="alert alert-success">
									     {{ Session::get( 'success' ) }}
									</div>
									@elseif( Session::has( 'warning' ))
									<div class="alert alert-danger">
									     {{ Session::get( 'warning' ) }} <!-- here to 'withWarning()' -->
									</div>

									@elseif($errors)
								     	@foreach($errors->all() as $error)
								     		<div class="alert alert-danger">
								     			{{ $error }} | Please choose section first.
								     		</div>
								     	@endforeach
									@endif


							    <form id="create_item_form" method="POST" action="{{route('item.store')}}" enctype="multipart/form-data">

							    	{!! csrf_field() !!}

							    	<div class="form-group">
									    <label for="inputSection">Type</label>
									   	<select name="type_id" class="form-control input-sm">
									   		@foreach($types as $type)
									   			<option value="{{$type->id}}">{{ $type->name }}</option>
									   		@endforeach
										</select>
									</div>

									<div class="form-group">
										<label for="inputItemName">Item Name</label>
    									<input type="text" class="form-control" name="name" id="inputItemName" placeholder="">
    								</div>

    								<div class="form-group">
										<label for="inputItemDescription">Item Description</label>
    									<textarea name="description" rows="4" cols="50"></textarea>
    								</div>

    								<div class="form-group">
									    <label for="inputCategory">Section</label>
									   	<select onChange="show_categories();" id="section_id" name="section_id" class="form-control input-sm">
									   		<option value="0">Please select property section</option>
									   		@foreach($sections as $section)
									   			<option value="{{ $section->id }}">{{ $section->name }}</option>
									   		@endforeach
										</select>
									</div>

                                    <div id="sectionloading" class="form-group" style="display:none">
                                        <center><img src="/img/sectionloading.gif" alt="" /></center>

                                    </div>

									<div id="category_view" style="display:none" class="form-group">
									    <label>Category</label>
									   	<select id="category_id" name="category_id" class="form-control input-sm">

										</select>
									</div>

									<div class="form-group">
										<label for="inputPrice">Price or Budget (Php)</label>
    									<input name="price" type="number" class="form-control" id="inputPrice" placeholder="">
    								</div>

    								<div class="form-group">
										<label >Image max: 1</label>
    									<input id="sample_input" name="images" type="file" class="form-control">
    								</div>

    								<div class="form-group">
    									<button type="submit" class="btn btn-primary">Post</button>
    								</div>

							    </form>

							</div>

						@elseif(!Auth::check())

							<div class="panel-heading">
							    <h3 class="panel-title">Please Login First.</h3>
							</div>

							<div class="panel-body">
							    <form role="form" method="post" action="{{ route('auth.login') }}">

									<input type="hidden" name="_token" value="{{ Session::token() }}">

									<div class="form-group">
									    <label for="username">Username:</label>
									    <input type="text" class="form-control input-sm" id="username" placeholder="Username" name="username">
									</div>

									<div class="form-group">
									    <label for="password">Password:</label>
									    <input type="password" class="form-control input-sm" id="password" placeholder="Password" name="password">
									</div>

									<button type="submit" class="btn btn-primary btn-xs">Login</button>
									<a class="btn btn-success btn-xs" href="{{ route('user.create') }}">Register</a>

									@if (session('info'))
		                                <span class="bg-warning">{{ session('info') }}</span>
		                            @endif

								</form>
							</div>

						@endif
						</div>



		  			</div>



				</div>

			</div>

			<div class="col-md-2">

					@include('layout.left-side')

			</div>

		</div>

	</div>

@endsection
