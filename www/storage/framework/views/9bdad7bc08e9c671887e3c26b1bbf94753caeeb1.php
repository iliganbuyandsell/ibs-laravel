				<?php if(!Auth::check()): ?>
				<a href="/redirect" class="btn btn-primary btn-sm btn-block"><i class="fa fa-facebook"></i> Facebook Login</a>

				<div class="panel panel-default">
					<div class="panel-heading">
					    <h3 class="panel-title">User Login</h3>
					</div>
					<div class="panel-body">

				    	<?php if(session('info')): ?>
                            <div class="alert alert-danger"><?php echo e(session('info')); ?></div>
                        <?php endif; ?>

					    <?php if($errors): ?>
					     	<?php foreach($errors->all() as $error): ?>
					     		<div class="alert alert-danger">
					     			<?php echo e($error); ?>

					     		</div>
					     	<?php endforeach; ?>
						<?php endif; ?>

						<form role="form" method="post" action="<?php echo e(route('auth.login')); ?>">

							<input type="hidden" name="_token" value="<?php echo e(Session::token()); ?>">

							<div class="form-group">
							    <label for="username">Username:</label>
							    <input type="text" class="form-control input-sm" id="username" placeholder="Username" name="username">
							</div>

							<div class="form-group">
							    <label for="password">Password:</label>
							    <input type="password" class="form-control input-sm" id="password" placeholder="Password" name="password">
							</div>

							<button type="submit" class="btn btn-primary btn-xs">Login</button>
							<a class="btn btn-success btn-xs" href="<?php echo e(route('user.create')); ?>">Register</a>

						</form>

					</div>
				</div>
				<?php endif; ?>

				<div class="panel panel-default">
					<div class="panel-heading">
					    <h3 class="panel-title">Sections</h3>
					</div>
					<div class="panel-body">

					    <ul id="section-list">

					    </ul>

					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
					    <h3 class="panel-title">Information</h3>
					</div>
					<div class="panel-body">



					</div>
				</div>
