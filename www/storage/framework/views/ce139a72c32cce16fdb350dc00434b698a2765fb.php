<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo e($pageTitle); ?></title>

	<link href="<?php echo e(asset('/css/bootstrap.min.css')); ?>" rel="stylesheet">

	<link href="<?php echo e(asset('/css/style.css')); ?>" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo e(url('/')); ?>"><img alt="Brand" width="20" height="20" src="<?php echo e(asset('/img/logo.png')); ?>"> <?php echo e(SITE_NAME); ?></a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="active"><a href="<?php echo e(url('/')); ?>">Home</a></li>
					<li><a href="<?php echo e(url('/')); ?>">Forums</a></li>
					<li><a href="<?php echo e(url('/about')); ?>">About Us</a></li>
				</ul>

				<form class="navbar-form navbar-left" role="search" method="GET" action="<?php echo e(url('/search')); ?>">
			        <div class="form-group">
			          <input name="query" type="text" class="form-control" placeholder="Search an item">
			        </div>
			        <button type="submit" class="btn btn-default btn-xs">Search</button>
			        <a class="btn btn-info btn-xs" href="<?php echo e(url('/item/create')); ?>"><i class="fa fa-plus-circle"></i> Post an item for free!</a>
			    </form>

			    

				<?php /* <ul class="nav navbar-nav navbar-right">
					<?php if(Auth::guest()): ?>
						<li><a href="<?php echo e(url('/auth/login')); ?>">Login</a></li>
						<li><a href="<?php echo e(route('item.create')); ?>">Register</a></li>
					<?php elseif(Auth::check()): ?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo e($user->name); ?> <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?php echo e(route('logout')); ?>">Logout</a></li>
							</ul>
						</li>
					<?php endif; ?>
				</ul> */ ?>

				<?php if(Auth::check()): ?>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							<span class="glyphicon glyphicon-briefcase"></span> 
							Inventory 
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?php echo e(url('/myitem', $user->id)); ?>">My Item(s)</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							<span class="glyphicon glyphicon-user"></span>
							<?php echo e(Auth::user()->name); ?> 
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="<?php echo e(route('user.show', Auth::user()->id)); ?>">Profile</a></li>
							<li><a href="<?php echo e(route('logout')); ?>">Logout</a></li>
						</ul>
					</li>
				</ul>
				<?php endif; ?>


			</div>
		</div>
	</nav>

	<?php echo $__env->yieldContent('content'); ?>

	<!-- Scripts -->
	<script src="<?php echo e(asset('/js/jquery.min.js')); ?>"></script>
	<script src="<?php echo e(asset('/js/bootstrap.min.js')); ?>"></script>
</body>
</html>
