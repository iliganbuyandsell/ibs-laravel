<?php $__env->startSection('content'); ?>

	<div class="container-fluid">
		
		<div class="row">
			
			<div class="col-md-10">

				<div class="row">
					
					<div class="col-md-12">

						<div class="alert alert-dismissible alert-info">
						  <!-- <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button> -->
						  <h4>Welcome to IBS | Iligan Buy and Sell!</h4>
						  <p>The best place to Buy a house, Sell a car or Find a job in Iligan City.</p>
						</div>


					</div>

				</div>
				
				<div class="row">
					
					<?php /* Selling Section / Categories Section */ ?>
		  			<div class="col-md-6">

		  				<div class="panel panel-default">
		  				<?php if(Auth::check()): ?>
							<div class="panel-heading">
							    <h3 class="panel-title">Item Information</h3>
							</div>
							<div class="panel-body">
							    	<?php if( Session::has( 'success' )): ?>
							    	<div class="alert alert-success">
									     <?php echo e(Session::get( 'success' )); ?>

									</div>
									<?php elseif( Session::has( 'warning' )): ?>
									<div class="alert alert-danger">
									     <?php echo e(Session::get( 'warning' )); ?> <!-- here to 'withWarning()' -->
									</div>
									<?php endif; ?>

								
							    <form method="POST" action="<?php echo e(route('item.store')); ?>" enctype="multipart/form-data">

							    	<?php echo csrf_field(); ?>


							    	<div class="form-group">
									    <label for="inputSection">Section</label>
									   	<select name="type_id" class="form-control input-sm">
									   		<?php foreach($types as $type): ?>
									   			<option value="<?php echo e($type->id); ?>"><?php echo e($type->name); ?></option>
									   		<?php endforeach; ?>
										</select>
									</div>

									<div class="form-group">
										<label for="inputItemName">Item Name</label>
    									<input type="text" class="form-control" name="name" id="inputItemName" placeholder="">
    								</div>

    								<div class="form-group">
										<label for="inputItemDescription">Item Description</label>
    									<textarea name="description" rows="4" cols="50"></textarea>
    								</div>

    								<div class="form-group">
									    <label for="inputCategory">Category</label>
									   	<select name="category_id" class="form-control input-sm">
									   		<option>Please select property category</option>
									   		<?php foreach($categories as $category): ?>
									   			<option value="<?php echo e($category->id); ?>"><?php echo e($category->name); ?></option>
									   		<?php endforeach; ?>
										</select>
									</div>

									<div class="form-group">
										<label for="inputPrice">Price or Budget (Php)</label>
    									<input name="price" type="number" class="form-control" id="inputPrice" placeholder="">
    								</div>

    								<div class="form-group">
										<label >Image max: 4</label>
    									<input name="images" type="file" class="form-control">
    								</div>

    								<div class="form-group">
    									<button type="submit" class="btn btn-primary">Post</button>
    								</div>

							    </form>

							</div>

						<?php elseif(!Auth::check()): ?>
					    <div class="alert alert-warning">
						  <strong>Warning!</strong> Please log in.
						</div>
						<?php endif; ?>
						</div>

						

		  			</div>
		  			<?php /* //Selling Section / Categories Section*/ ?>

		  			<?php /* Buying Section */ ?>
		  			<div class="col-md-6">

		  				<div class="panel panel-warning">
							<div class="panel-heading">
							    <h3 class="panel-title">Basic Posting Item Guideline</h3>
							</div>
							<div class="panel-body">
							    <p>You can list as many items as you want in the buy/sell section. Here are the requirements and recommendations for adding product images, descriptions and variants.</p>
							    <ul>
							    	
							    	<li>At least one image is required for each product. Are easy to understand and show the whole product</li>
							    	<li>Description, should be rich text only (no HTML). Provide only information directly related to the product. Make use of short sentences.</li>
							    	<li>You will not post items in the a section that do not fall under any of the defined categories.</li>
							    	<li>By posting items in the IBS, you guarantee that your item has been legally purchased or acquired. IBS will not be liable for any false, misleading or inaccurate representations by its users.</li>

							    </ul>
							</div>
						</div>

						

		  			</div>
		  			<?php /* //Buying Section */ ?>

				</div>

			</div>

			<div class="col-md-2">

					<?php echo $__env->make('layout.left-side', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					
			</div>

		</div>

	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>