				
				<a href="/redirect" class="btn btn-primary btn-sm btn-block"><i class="fa fa-facebook"></i> Facebook Login</a>	
			
				<?php if(!Auth::check()): ?>
				<div class="panel panel-default">
					<div class="panel-heading">
					    <h3 class="panel-title">User Login</h3>
					</div>
					<div class="panel-body">
					    
					    
						<form role="form" method="post" action="<?php echo e(route('auth.login')); ?>">
							
							<input type="hidden" name="_token" value="<?php echo e(Session::token()); ?>">

							<div class="form-group">
							    <label for="username">Username:</label>
							    <input type="text" class="form-control input-sm" id="username" placeholder="Username" name="username">
							</div>

							<div class="form-group">
							    <label for="password">Password:</label>
							    <input type="password" class="form-control input-sm" id="password" placeholder="Password" name="password">
							</div>

							<button type="submit" class="btn btn-primary btn-xs">Login</button>
							<a class="btn btn-success btn-xs" href="<?php echo e(route('user.create')); ?>">Register</a>

							<?php if(session('info')): ?>
                                <span class="bg-warning"><?php echo e(session('info')); ?></span>
                            <?php endif; ?>

						</form>
						
					</div>
				</div>
				<?php endif; ?>

				<div class="panel panel-default">
					<div class="panel-heading">
					    <h3 class="panel-title">Categories</h3>
					</div>
					<div class="panel-body">
					    
					    <ul>
					    	<li>Real Estate</li>
					    	<li>Cars</li>
					    	<li>Mobile</li>
					    	<li>Computers</li>
					    	<li>Clothes</li>
					    	<li>Pets</li>
					    	<li>Hobbies</li>
					    	<li>Motorcycles</li>
					    	<li>Appliances</li>
					    	<li>Furniture</li>
					    	<li>Baby Stuff</li>
					    	<li>Services</li>
					    	<li>Jobs</li>
					    	<li>Others</li>
					    </ul>
						
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
					    <h3 class="panel-title">Information</h3>
					</div>
					<div class="panel-body">
					    
					    
						
					</div>
				</div>