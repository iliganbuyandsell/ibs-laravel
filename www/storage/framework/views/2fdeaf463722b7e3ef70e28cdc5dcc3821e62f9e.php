    <!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">

		<!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo e(asset("/bower_components/admin-lte/dist/img/user2-160x160.png")); ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?php echo e(Auth::user()->name); ?></p>
                <!-- Status -->
                <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="<?php echo e(url('/search')); ?>" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="query" class="form-control" placeholder="Search..."/>
          		<span class="input-group-btn">
            		<button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
          		</span>
            </div>
        </form>
        <!-- /.search form -->
		<!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form" onsubmit="return false">
            <div class="input-group">
				<select class="form-control" name="select-section" id="select-section">
				</select>
          		<span class="input-group-btn">
            		<button onclick="showItemBySection();" name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
          		</span>
            </div>
        </form>
        <!-- /.search form -->

		<!-- Sidebar Menu -->
        <ul class="sidebar-menu">
        	<li class="header">Main Navigation</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="<?php echo e(url('/')); ?>"><i class="fa fa-home"></i> <span>Home</span></a></li>
			<li><a href="<?php echo e(route('item.create')); ?>"><i class="fa fa-plus"></i> <span>Post an Item</span></a></li>
			<li><a href="<?php echo e(url('/myitem', Auth::user()->id)); ?>"><i class="fa fa-check-square"></i> <span>Inventory</span></a></li>
            <!-- <li><a href="<?php echo e(url('/')); ?>"><i class="fa fa-forumbee"></i> <span>Forums</span></a></li> -->
            <li><a href="<?php echo e(url('/about')); ?>"><i class="fa fa-info"></i> <span>About Us</span></a></li>
			<li><a href="<?php echo e(route('forums.index')); ?>"><i class="fa fa-info"></i> <span>Open Forums</span></a></li>
        </ul><!-- /.sidebar-menu -->

	</aside>
