<?php $__env->startSection('content'); ?>

	<div class="container-fluid">

		<div class="row">

			<div class="col-md-10">

				<div class="row">

					<?php /* Selling Section / Categories Section */ ?>
		  			<div class="col-md-12">

		  				<div class="panel panel-default">
							<div class="panel-heading">
							    <h3 class="panel-title">Item Information</h3>
							</div>
							<div class="panel-body">

							    <div class="col-md-6">
									<h1><?php echo e($item->name); ?></h1>
									<p>Price: ₱ <?php echo e($item->price); ?></p>
									<p>Posted by: <a href="<?php echo e(route('user.show', $item->user->id)); ?>"><?php echo e($item->user->name); ?></a> | <?php echo e($item->created_at); ?></p><br>
									<p>
										<h3>Description</h3>
										<?php echo e($item->description); ?>

									</p>
								</div>

								<div class="col-md-6">
								<?php if($images!=null): ?>
									<img width="304" height="236" src="<?php echo e(asset('/www/public/img/'.$images->name)); ?>">
								<?php endif; ?>
								</div>

							</div>
						</div>

						<div class="panel panel-default">
							<div class="panel-heading">
							    <h3 class="panel-title">Comment(s)</h3>
							</div>
							<div class="panel-body">
							    <?php if(Auth::check()): ?>
									<form method="POST" action="<?php echo e(route('comment.item.user.store', [$item->id, Auth::user()->id])); ?>">

								    	<?php echo csrf_field(); ?>


										<div class="form-group">
	    									<input name="comment" type="text" class="form-control" placeholder="write comment...">
	    									<button type="submit" class="btn btn-primary">Post</button>
	    								</div>

								    </form>
								<?php else: ?> <p>Please Log in to post comment(s)</p>
							    <?php endif; ?>

							    <?php foreach($comments as $comment): ?>
							    	<p><?php echo e($comment->user->name); ?> says: <?php echo e($comment->comment); ?></p>
							    <?php endforeach; ?>

							</div>
						</div>



		  			</div>
		  			<?php /* //Selling Section / Categories Section*/ ?>


				</div>

			</div>

			<div class="col-md-2">

					<?php if(!Auth::check()): ?>

					<?php echo $__env->make('layout.left-side', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

					<?php else: ?>
					<?php echo $__env->make('layout.left-side', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

					<?php endif; ?>


			</div>

		</div>

	</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout.lte-default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>