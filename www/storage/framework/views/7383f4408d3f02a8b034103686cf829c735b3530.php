<?php $__env->startSection('content'); ?>

	<div class="container-fluid">
		
		<div class="row">

			<div class="col-md-10">

				<div class="row">
					
					<div class="col-md-12">

						<div class="alert alert-dismissible alert-info">
						  <!-- <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button> -->
						  <h4>Welcome to IBS | Iligan Buy and Sell!</h4>
						  <p>The best place to Buy a house, Sell a car or Find a job in Iligan City.</p>

							<?php if( Session::has( 'success' )): ?>
							    <div class="alert alert-success">
									<?php echo e(Session::get( 'success' )); ?>

								</div>
							<?php endif; ?>

						</div>


					</div>

				</div>
				
				<div class="row">
					
					<?php /* Selling Section / Categories Section */ ?>
		  			<div class="col-md-6">

		  				<div class="panel panel-success">
							<div class="panel-heading">
							    <h3 class="panel-title">Selling Section</h3>
							</div>
							<div class="panel-body ibs-panel-body">

										



								<?php foreach($for_sell as $fs): ?>

									<div class="media">
										
										<div class="media-left">
											<a href="<?php echo e(route('item.show', $fs->id)); ?>">
												<?php foreach($images as $image): ?>
													<?php if($image->item_id == $fs->id): ?>
														<img class="media-object" data-src="<?php echo e(asset('/www/public/img/'.$image->name)); ?>" alt="Description Here" src="<?php echo e(asset('/www/public/img/'.$image->name)); ?>" width="64" height="64">
													<?php endif; ?>
												<?php endforeach; ?>
											</a>
										</div>

										<div class = "media-body">
											<h4 class = "media-heading"><a href="<?php echo e(route('item.show', $fs->id)); ?>"><?php echo e($fs->name); ?></a></h4>
											<p><?php echo e($fs->description); ?></p>
											<small>
												<em>
													<span class="price"><?php echo e($fs->price); ?></span>
													Posted by: <span class="posted_by"><?php echo e($fs->user->name); ?></span>
													Created at: <span class="created_at"><?php echo e(date_format($fs->created_at,"F j, Y g:i a")); ?></span>
												</em>
											</small>
										</div>

									</div>

								<?php endforeach; ?>

								<div class="pagination"> <?php echo e($for_sell->links()); ?> </div>

							</div>
						</div>

						

		  			</div>
		  			<?php /* //Selling Section / Categories Section*/ ?>

		  			<?php /* Buying Section */ ?>
		  			<div class="col-md-6">

		  				<div class="panel panel-info">
							<div class="panel-heading">
							    <h3 class="panel-title">Buying Section</h3>
							</div>
							<div class="panel-body ibs-panel-body">
							    <?php foreach($looking_for as $lf): ?>

									<div class="media">
										
										<div class="media-left">
											<a href="<?php echo e(route('item.show', $lf->id)); ?>">
												<?php foreach($images as $image): ?>
													<?php if($image->item_id == $lf->id): ?>
														<img class="media-object" data-src="<?php echo e(asset('/www/public/img/'.$image->name)); ?>" alt="Description Here" src="<?php echo e(asset('/www/public/img/'.$image->name)); ?>" width="64" height="64">
													<?php endif; ?>
												<?php endforeach; ?>
											</a>
										</div>

										<div class = "media-body">
											<h4 class = "media-heading"><a href="<?php echo e(route('item.show', $lf->id)); ?>"><?php echo e($lf->name); ?></a></h4>
											<p><?php echo e($lf->description); ?></p>
											<small>
												<em>
													<span class="price"><?php echo e($lf->price); ?></span>
													Posted by: <span class="posted_by"><?php echo e($lf->user->name); ?></span>
													Created at: <span class="created_at"><?php echo e(date_format($lf->created_at,"F j, Y g:i a")); ?></span>
												</em>
											</small>
										</div>

									</div>
										
								<?php endforeach; ?>

								<div class="pagination"> <?php echo e($looking_for->links()); ?> </div>
							</div>
						</div>

						

		  			</div>
		  			<?php /* //Buying Section */ ?>

				</div>

			</div>

			<div class="col-md-2">

					<?php echo $__env->make('layout.left-side', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

			</div>

		</div>

	</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>