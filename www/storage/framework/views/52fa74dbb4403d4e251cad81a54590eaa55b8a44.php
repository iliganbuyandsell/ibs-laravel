<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo e($pageTitle); ?></title>
	<!-- Tell the browser to be responsive to screen width -->
  	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

	<!-- Bootstrap 3.3.6 -->
	<link href="<?php echo e(asset("/bower_components/admin-lte/bootstrap/css/bootstrap.min.css")); ?>" rel="stylesheet" type="text/css" />

	<!-- Font Awesome -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

	<link href="<?php echo e(asset('/components/imgareaselect/css/imgareaselect-default.css')); ?>" rel="stylesheet" media="screen">

	<!-- Ionicons -->
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

  	<!-- Theme style -->
    <link href="<?php echo e(asset("/bower_components/admin-lte/dist/css/AdminLTE.min.css")); ?>" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
              page. However, you can choose any other skin. Make sure you
              apply the skin class to the body tag so the changes take effect.
        -->

    <link href="<?php echo e(asset("/bower_components/admin-lte/dist/css/skins/skin-blue.min.css")); ?>" rel="stylesheet" type="text/css" />


	<link rel="stylesheet" href="<?php echo e(asset('/css/jquery.awesome-cropper.css')); ?>">


	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

	<link href="<?php echo e(asset('/css/style.css')); ?>" rel="stylesheet">

	<style media="screen">
		#item-description {
			white-space: nowrap;
		    width: 12em;
		    overflow: hidden;
		    text-overflow: ellipsis;
		}
	</style>

</head>
<body class="skin-blue sidebar-mini" data-spy="scroll" data-target="#scrollspy">

<div class="wrapper">

	<!-- Main Header -->
	<?php echo $__env->make('layout.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<!-- Left side column. contains the logo and sidebar -->
	<?php echo $__env->make('layout.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">

		<!-- Content Header (Page header) -->
        <section class="content-header">

        	<!-- Content Header (Page header) -->
            <h1>
                Welcome to Iligan Buy and Sell
                <small>The best place to Buy a house, Sell a car or Find a job in Iligan City.</small>
            </h1>
            <!-- <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol> -->


        </section>

        <!-- Main content -->
        <section class="content">

           	<?php echo $__env->yieldContent('content'); ?>

        </section>

	</div><!-- /.content-wrapper -->

	<!-- Main Footer -->
    <?php echo $__env->make('layout.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


</div>

	<!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.3 -->
    <script src="<?php echo e(asset ("/bower_components/admin-lte/plugins/jQuery/jQuery-2.2.0.min.js")); ?>"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo e(asset ("/bower_components/admin-lte/bootstrap/js/bootstrap.min.js")); ?>" type="text/javascript"></script>
    <!-- AdminLTE App -->

    <script src="<?php echo e(asset ("/bower_components/admin-lte/dist/js/app.js")); ?>" type="text/javascript"></script>

	<!-- Scripts -->
	<script src="<?php echo e(asset('/js/main.js')); ?>"></script>
	<script src="<?php echo e(asset('/components/imgareaselect/scripts/jquery.imgareaselect.js')); ?>"></script>
	<script src="<?php echo e(asset('/build/jquery.awesome-cropper.js')); ?>"></script>

</body>
</html>
